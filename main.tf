
resource "google_compute_disk" "disk-1" {
  name = "disk-size-10"
  size = 10
  zone = "${var.current-zone}"
}

resource "google_compute_disk" "disk-2" {
  name = "disk-size-8"
  size = 8
  zone = "${var.current-zone}"
}

resource "google_compute_address" "mystaticip" {
  name = "my-ipv4-address"
}

resource "google_compute_instance" "my-servers" {
  count = "${var.node_count}"
  name         = "node${count.index+1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params {
      size = "20"
      image = "debian-cloud/debian-10"
    }
  }
  attached_disk {
    source = google_compute_disk.disk-1.name
  }
  attached_disk {
    source = google_compute_disk.disk-2.name
  }

  metadata = {
    ssh-keys = "devopssam21:${file("~/.ssh/id_rsa.pub")}"
  }
  connection {
    type     = "ssh"
    user     = "devopssam21"
    private_key = "${file(var.private_key_path)}"
    host     = "${self.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt install curl -y",
#      "sudo rm -rf /var/www/html/*",
#      "sudo chmod 777 /var/www/html",
      "sudo echo '<b> Juneway ${self.network_interface.0.network_ip} ${self.name}</b>' > /home/devopssam21/index.html",
      "curl -sSL https://get.docker.com/ | sh",
      "sudo usermod -aG docker $(whoami)",
      "sudo docker run -d -p 80:80 --name mynginx -v /home/devopssam21/:/usr/share/nginx/html/ nginx" 
    ]
  }
  provisioner "local-exec" {
   command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }
  

  network_interface {
    network = "default"
    access_config {
    nat_ip = google_compute_address.mystaticip.address
    }
  }
}

resource "cloudflare_record" "vdemyankov" {
  zone_id = "38a3f690d0be85a323ae19af19fd01e8"
  name = "vdemyankov.juneway.pro"
  value = google_compute_address.mystaticip.address
  type = "A"
  ttl  = 3600
}
output "hostname_projectid" {
   value = {
    for info in google_compute_instance.my-servers: 
    info.name => info.project
   }
}


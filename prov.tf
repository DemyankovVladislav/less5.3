provider "google" {
  credentials = file("juneway-335712-51a94a0932a7.json")
  project     = "juneway-335712"
  region      = "europe-west3"
  zone        = "${var.current-zone}"
}

terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "cloudflare" {
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}
